# Product Management System 
## Clone the project 
````
git clone git@gitlab.com:niranjan.shrestha/productmanagement.git
````
## Goto project directory
````
cd productmanagement
````
## Copy .env file from .env.example

````
cp .env.example .env
````

## Configure env file
## Install composer

````
composer install
````
## Generate application key
````
php artisan key:generate
````
## Run migration
````
php artisan module:migrate
```` 
## Run seeder
````
php artisan module:seed
````
## Run the application
````
php artisan serve
````
