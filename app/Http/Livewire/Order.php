<?php
namespace App\Http\Livewire;

use Livewire\Component;
use Modules\Order\Entities\Cart;
use Brian2694\Toastr\Facades\Toastr;
use Modules\Product\Entities\Product;

class Order extends Component
{
    public $products = [];

    public $product_code;

    public $message;

    public $productInCart;

    public $paidMoney = '';

    public $returnedMoney = '';

    public function mount()
    {
        $this->products = Product::all();

        $this->productInCart = Cart::all();
    }

    public function insertToCart()
    {
        $findProduct = Product::where('product_code', $this->product_code)->first();

        if (!$findProduct) {
            Toastr::error('Product not found.');

            return redirect(route('order.index'));
        }

        if ($findProduct->stock == 0) {
            Toastr::error('Out of stock.');

            return redirect(route('order.index'));
        }

        $countCartProduct = Cart::where('product_id', $findProduct->id)->count();

        if ($countCartProduct > 0) {
            Toastr::success('Product already added. Change the qunatity.');

            return redirect(route('order.index'));
        } else {
            $addToCart = new Cart;
            $addToCart->product_id = $findProduct->id;
            $addToCart->product_quantity = 1;
            $addToCart->product_price = $findProduct->price;
            // $addToCart->user_id = auth()->user()->id;
            $addToCart->save();

            $this->productInCart->push($addToCart);

            $this->product_code = ' ';

            Toastr::success('Product added successfully.');

            return redirect(route('order.index'));
        }
    }

    public function removeProduct($cartId)
    {
        $deleteCart = Cart::find($cartId);

        $deleteCart->delete();

        Toastr::success('Product has removed from cart.');

        $this->productInCart = $this->productInCart->except($cartId);

        return redirect(route('order.index'));
    }

    public function incrementQuantity($cartId)
    {
        $cart = Cart::find($cartId);

        $stock = $cart->product->stock;

        if ($cart->product_quantity == $stock) {
            Toastr::warning('Out of stock.');

            return redirect(route('order.index'));
        } else {
            $cart->increment('product_quantity', 1);

            $updatedPrice = $cart->product_quantity * $cart->product->price;

            $cart->update([
                'product_price' => $updatedPrice,
            ]);

            $this->mount();
        }
    }

    public function decrementQuantity($cartId)
    {
        $cart = Cart::find($cartId);

        if ($cart->product_quantity == 1) {
            return $this->message = 'Order product can\'t be less than one. ';
        }

        $cart->decrement('product_quantity', 1);

        $updatedPrice = $cart->product_quantity * $cart->product->price;

        $cart->update([
            'product_price' => $updatedPrice,
        ]);

        $this->mount();
    }

    public function render()
    {
        if ($this->paidMoney !== '') {
            $totalReturnedAmount = $this->paidMoney - $this->productInCart->sum('product_price');

            $this->returnedMoney = $totalReturnedAmount;
        }

        return view('livewire.order');
    }
}
