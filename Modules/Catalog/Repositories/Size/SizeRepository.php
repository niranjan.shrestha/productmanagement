<?php
namespace Modules\Catalog\Repositories\Size;

use Modules\Catalog\Entities\Size;

class SizeRepository implements SizeInterface
{
    public function getAllSizes()
    {
        return Size::all();
    }
}
