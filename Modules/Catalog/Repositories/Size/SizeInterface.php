<?php
namespace Modules\Catalog\Repositories\Size;

interface SizeInterface
{
    public function getAllSizes();
}
