<?php
namespace Modules\Catalog\Repositories\Brand;

use Modules\Catalog\Entities\Brand;

class BrandRepository implements BrandInterface
{
    public function getAllBrands()
    {
        return Brand::all();
    }

    public function findBrand($id)
    {
        return Brand::findOrFail($id);
    }

    public function countAllBrands()
    {
        return Brand::all()->count();
    }
}
