<?php
namespace Modules\Catalog\Repositories\Brand;

interface BrandInterface
{
    public function getAllBrands();

    public function findBrand($id);

    public function countAllBrands();
}
