<?php
namespace Modules\Catalog\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Catalog\Entities\Brand;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Contracts\Support\Renderable;
use Modules\Catalog\Repositories\Brand\BrandInterface;
use Modules\Catalog\Http\Requests\Brand\CreateBrandRequest;
use Modules\Catalog\Http\Requests\Brand\UpdateBrandRequest;

class BrandController extends Controller
{
    protected $brandRepository;

    public function __construct(BrandInterface $brandRepository)
    {
        $this->brandRepository = $brandRepository;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $allBrands = $this->brandRepository->getAllBrands();

        return view('catalog::brands.index', ['brands' => $allBrands]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('catalog::brands.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(CreateBrandRequest $request, Brand $brand)
    {
        $brand->create($request->all());

        Toastr::success('Brand added successfully :)');

        return redirect(route('brand.index'));
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('catalog::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $brand = $this->brandRepository->findBrand($id);

        return view(
            'catalog::brands.create',
            [
                'brand' => $brand
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     * @param UpdateBrandRequest $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateBrandRequest $request, $id)
    {
        $brand = $this->brandRepository->findBrand($id);

        $brand->update($request->all());

        Toastr::success('Brand updated successfully :)');

        return redirect(route('brand.index'));
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $brand = $this->brandRepository->findBrand($id);

        $brand->delete();

        Toastr::success('Brand deleted successfully :)');

        return redirect(route('brand.index'));
    }
}
