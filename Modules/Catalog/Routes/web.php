<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::prefix('catalog')->middleware(['auth'])->group(function () {
//     Route::get('/', ['as' => 'catalog.index', 'uses' => 'CatalogController@index']);
// });

Route::group(['prefix' => 'brand', 'middleware' => 'auth'], function () {
    Route::get('/', ['as' => 'brand.index', 'uses' => 'BrandController@index']);

    Route::get('create', ['as' => 'brand.create', 'uses' => 'BrandController@create']);

    Route::post('store', ['as' => 'brand.store', 'uses' => 'BrandController@store']);

    Route::get('edit/{id}', ['as' => 'brand.edit', 'uses' => 'BrandController@edit']);

    Route::put('update/{id}', ['as' => 'brand.update', 'uses' => 'BrandController@update']);

    Route::get('destroy/{id}', ['as' => 'brand.destroy', 'uses' => 'BrandController@destroy']);
});
