@extends('admin::layouts.master')

@section('title')
{{ isset($brand) ? 'Edit' : 'Add'}} brand
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <div class="card-head">
                <header>{{ isset($brand) ? 'Update' : 'Add' }} brand</header>
            </div>

            <form
                action="{{ isset($brand) ? route('brand.update', $brand->id) : route('brand.store') }}"
                method="post"
            >

                @csrf

                @if (isset($brand))

                @method('PUT')

                @endif

                <div class="card-body row">

                    <div class="col-lg-12 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                            <input
                                class="mdl-textfield__input"
                                type="text"
                                id="name"
                                name="name"
                                value="{{ isset($brand) ? $brand->name : old('name') }}"
                            >
                            <label class="mdl-textfield__label">Brand Name</label>
                        </div>

                        @error('name')
                        <span class="text-danger">
                            {{ $message }}
                        </span>
                        @enderror

                    </div>

                    <div class="col-lg-12 p-t-20 text-center">
                        <button
                            type="submit"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink"
                        >Submit</button>

                        <a
                            href="{{ route('brand.index') }}"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default"
                        >Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
