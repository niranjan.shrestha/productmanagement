@extends('catalog::layouts.master')

@section('title')
All brands
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-topline-red">
            <div class="card-head">
                <header>
                    All Brands
                </header>
            </div>
            <div class="card-body ">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-6">
                        <div class="btn-group">
                            <a
                                href="{{ route('brand.create') }}"
                                id="addRow1"
                                class="btn btn-info"
                            >
                                Add New <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="table-scrollable">
                    <table
                        class="table table-striped table-bordered table-hover table-checkable order-column"
                        style="width: 100%"
                        id="example4"
                    >
                        <thead>
                            <tr>
                                <th> S.N. </th>
                                <th> Brand name </th>
                                <th width="9%"> Actions </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($brands as $brand)

                            <tr>

                                <td>
                                    {{ $loop->iteration }}
                                </td>

                                <td>
                                    {{ $brand->name }}
                                </td>

                                <td class="d-flex justify-content-center">
                                    <a
                                        href="{{ route('brand.edit', $brand->id) }}"
                                        class="btn btn-xs btn-info"
                                    >
                                        Edit
                                    </a>

                                    <!-- Button trigger modal -->
                                    <a
                                        class="btn btn-xs btn-danger deleteBrand"
                                        data-toggle="modal"
                                        data-target="#deleteBrand"
                                        link="{{ route('brand.destroy', $brand->id) }}"
                                    >
                                        Delete
                                    </a>
                                </td>

                            </tr>

                            <!-- Delete Modal -->
                            <div
                                class="modal fade"
                                id="deleteBrand"
                                tabindex="-1"
                                role="dialog"
                                aria-labelledby="exampleModalCenterTitle"
                                aria-hidden="true"
                            >
                                <div
                                    class="modal-dialog modal-dialog-centered"
                                    role="document"
                                >
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5
                                                class="modal-title"
                                                id="exampleModalLongTitle"
                                            >
                                                Are you sure?
                                            </h5>
                                            <button
                                                type="button"
                                                class="close"
                                                data-dismiss="modal"
                                                aria-label="Close"
                                            >
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <strong> {{ $brand->name }} </strong> will be removed. This action can't be
                                            reverted, be careful.
                                        </div>
                                        <div class="modal-footer">
                                            <a class="btn btn-danger getDeleteLink">
                                                Yes, Delete it.
                                            </a>

                                            <button
                                                type="button"
                                                class="btn btn-secondary"
                                                data-dismiss="modal"
                                            >
                                                Go Back
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script>
    $('.deleteBrand').on('click', function(){
        var getDeleteLink = $(this).attr('link');

        $('.getDeleteLink').attr('href', getDeleteLink);
    });
</script>

@endsection
