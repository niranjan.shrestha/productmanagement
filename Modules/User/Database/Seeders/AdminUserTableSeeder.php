<?php
namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\User\Entities\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;

class AdminUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $userExists = User::where('username', 'Admin')->first();

        if (!$userExists) {
            User::create([
                'username' => 'Admin',
                'email' => 'admin@pms.com',
                'password' => Hash::make('admin')
            ]);
        }

        $this->command->alert('Please use username as Admin and password as admin.');
    }
}
