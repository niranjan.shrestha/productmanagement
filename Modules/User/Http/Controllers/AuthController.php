<?php
namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Support\Renderable;

class AuthController extends Controller
{
    /**
     *  LOG IN SECTION
     */
    public function login()
    {
        return view('user::auth.login');
    }

    public function check(Request $request)
    {
        if (auth()->attempt(['username' => $request->username, 'password' => $request->password])) {
            $request->session()->regenerate();

            Toastr::success('Welcome back. :)');

            return redirect()->intended(route('admin.dashboard'));
        } else {
            Toastr::info('I think you mistyped. Please try again.');

            return redirect(route('auth.login'));
        }
    }

    public function logout()
    {
        Auth::logout();

        Toastr::success('See you soon. :)');

        return redirect()->intended(route('auth.login'));
    }
}
