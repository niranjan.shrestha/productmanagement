<?php
namespace Modules\Product\Entities;

use Modules\Order\Entities\Cart;
use Illuminate\Database\Eloquent\Model;
use Modules\Category\Entities\Category;
use Modules\Order\Entities\OrderDetail;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
        'brand_name',
        'category_id',
        'image',
        'price',
        'product_code',
        'product_name',
        'stock',
    ];

    /**
     * Get all of the orderDetail for the Product
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderDetail()
    {
        return $this->hasMany(OrderDetail::class);
    }

    /**
     * Get the cart that owns the Product
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function cart()
    {
        return $this->hasMany(Cart::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
