@extends('product::layouts.master')

@section('title')
All products list
@endsection

@section('content')

<script>
    function switch_div(show) {
    document.getElementById("show_" + show).style.display = "block";
    document.getElementById("show_" + (show == 1 ? 2 : 1)).style.display =
        "none";
}
</script>

<!-- start product list -->
<div class="row">
    <div class="col-md-12">
        <div class="tabbable-line">
            <ul
                class="nav customtab nav-tabs mb-3"
                role="tablist"
            >
                <li class="nav-item">
                    <a
                        onclick="switch_div(1)"
                        class="nav-link  active"
                        data-toggle="tab"
                    >
                        List View
                    </a>
                </li>

                <li class="nav-item">
                    <a
                        onclick="switch_div(2)"
                        class="nav-link "
                        data-toggle="tab"
                    >
                        Grid View
                    </a>
                </li>
            </ul>


            <div
                class="card-box"
                @if(count($products)
            > 0)
                style="width:100%;"
                @endif>
                <div class="card-head">
                    <header>
                        All products
                    </header>
                </div>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="">
                                <div class="tab-content">
                                    <div
                                        class="tab-pane active  fontawesome-demo"
                                        id="show_1"
                                    >

                                        <!-- start products list -->
                                        <div class="table-scrollable">
                                            <table
                                                class="table table-striped table-bordered table-hover table-checkable order-column valign-middle"
                                                id="example4"
                                            >
                                                <thead>
                                                    <tr>
                                                        <th> S.N. </th>
                                                        <th> Image </th>
                                                        <th> Product name </th>
                                                        <th> Category </th>
                                                        <th> Stock </th>
                                                        <th> Price </th>
                                                        <th> Product Code </th>
                                                        <th width="10%"> Action </th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    @foreach ($products as $product)
                                                    <tr class="odd gradeX">
                                                        <td class="left">
                                                            {{ $loop->iteration }}
                                                        </td>

                                                        <td class="patient-img">
                                                            <img
                                                                src="{{ asset('products/uploads/' . $product->image) }}"
                                                                alt=""
                                                            >
                                                        </td>

                                                        <td>
                                                            {{ substr_replace($product->product_name, '...', 20) }}
                                                        </td>

                                                        <td class="left">
                                                            {{ $product->category->name }}
                                                        </td>

                                                        <td>

                                                            @if ($product->stock == 0)
                                                            <span class="text text-danger">
                                                                <b class="text-center">
                                                                    Out of stock.
                                                                </b>
                                                            </span>

                                                            @elseif (
                                                            $product->stock <
                                                                10)
                                                                <span
                                                                class="text text-danger"
                                                            >
                                                                Low stock >> {{ $product->stock }}
                                                                </span>

                                                                @else
                                                                <b>
                                                                    {{ $product->stock }}
                                                                </b>

                                                                @endif
                                                        </td>

                                                        <td>
                                                            Rs. {{ $product->price }}
                                                        </td>

                                                        <td class="left">
                                                            {{ $product->product_code }}
                                                        </td>

                                                        <td>
                                                            <a
                                                                href="{{ route('product.edit', $product->id) }}"
                                                                class="btn btn-primary btn-xs"
                                                                title="Edit"
                                                            >
                                                                <i class="fa fa-pencil"></i>
                                                            </a>

                                                            <!-- Button trigger modal -->
                                                            <a
                                                                class="btn btn-danger btn-xs deleteProduct"
                                                                title="Delete"
                                                                data-toggle="modal"
                                                                data-target="#deleteProduct"
                                                                link="{{ route('product.delete', $product->id) }}"
                                                            >
                                                                <i class="fa fa-trash-o "></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- End products list -->
                                    </div>

                                    <!-- Grid view starts from here-->
                                    <div
                                        class="tab-pane  "
                                        id="show_2"
                                    >
                                        <!-- start products list -->
                                        <div class="row">
                                            @if (count($products) > 0)
                                            @foreach ($products as $product)
                                            <div class="col-lg-3 col-md-6 col-12 col-sm-6">
                                                <div class="blogThumb">
                                                    <div class="thumb-center">
                                                        <img
                                                            class="img-responsive"
                                                            alt="{{ $product->image }}"
                                                            src="{{ asset('products/uploads/' .$product->image) }}"
                                                            style="height: 230px;"
                                                        />
                                                    </div>
                                                    <div class="course-box">
                                                        <h4>{{ substr_replace($product->product_name, '...', 20) }}</h4>
                                                        <div class="text-muted"><span class="m-r-10">Sep 23</span>
                                                        </div>
                                                        <p>
                                                            <span class="text
                                                            @if ($product->stock < 10)
                                                                text-danger bold
                                                            @endif
                                                            ">
                                                                <i class="fa fa-bar-chart-o"></i>
                                                                Stock: {{ $product->stock }}
                                                            </span>
                                                        </p>

                                                        <p>
                                                            <span>
                                                                <i class="fa fa-bolt"></i>
                                                                Brand: {{ $product->brand_name }}
                                                            </span>
                                                        </p>

                                                        <p>
                                                            <span>
                                                                <i class="fa fa-money"></i>
                                                                Price: <b> Rs. {{ $product->price }} </b>
                                                            </span>
                                                        </p>

                                                        <a
                                                            href="{{ route('product.show', $product->id) }}"
                                                            type="button"
                                                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-info"
                                                        >Details</a>
                                                    </div>
                                                </div>
                                            </div>

                                            @endforeach
                                            @else
                                            <h1 class="text-center">
                                                No record found create yet. Please add to view your all products.
                                            </h1>
                                            @endif
                                        </div>
                                        <!-- End course list -->


                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End product list -->

<!-- Modal -->
<div
    class="modal fade"
    id="deleteProduct"
    tabindex="-1"
    role="dialog"
    aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true"
>
    <div
        class="modal-dialog modal-dialog-centered"
        role="document"
    >
        <div class="modal-content">
            <div class="modal-header">
                <h5
                    class="modal-title"
                    id="exampleModalLongTitle"
                >
                    <b>
                        Be careful, what you wish for.
                    </b>
                </h5>
                <button
                    type="button"
                    class="close"
                    data-dismiss="modal"
                    aria-label="Close"
                >
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                This action will permanently removes product from the database. Are you sure?
            </div>
            <div class="modal-footer">

                <a
                    type="button"
                    class="btn btn-danger getProductDeleteLink"
                    href=""
                >
                    Yes, Delete it.
                </a>

                <button
                    type="button"
                    class="btn btn-secondary"
                    data-dismiss="modal"
                >
                    No, Go back.
                </button>

            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

<script>
    $('.deleteProduct').on('click', function(){
    var deleteProductLink = $(this).attr('link');

    $('.getProductDeleteLink').attr('href', deleteProductLink);
});
</script>

@endsection
