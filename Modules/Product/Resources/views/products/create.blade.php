@extends('product::layouts.master')

@section('title')
Create a product
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <div class="card-head">
                <header>
                    {{ isset($product) ? 'Edit' : 'Create' }} a product
                </header>
            </div>

            <form
                action="{{ isset($product) ? route('product.update', $product->id) : route('product.store') }}"
                method="post"
                enctype="multipart/form-data"
            >

                @csrf

                @if (isset($product))

                @method('PUT')

                @endif

                <div class="card-body row">

                    <!-- product name -->
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                            <input
                                class="mdl-textfield__input"
                                type="text"
                                id="product_name"
                                name="product_name"
                                value="{{ isset($product) ? $product->product_name : old('product_name') }}"
                            >
                            <label class="mdl-textfield__label">Product Name</label>
                        </div>

                        @error('product_name')
                        <span class="text text-danger">
                            {{ $message }}
                        </span>
                        @enderror

                    </div>

                    <!-- brand name -->
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                            <select
                                class="form-control"
                                name="brand_name"
                                id=""
                            >

                                <option value="">Please select a brand.</option>

                                @foreach($brands as $brand)

                                <option
                                    value="{{ $brand->name }}"

                                    @if(isset($product) && $product->brand_name == $brand->name)
                                    selected
                                    @endif

                                    >

                                    {{ $brand->name }}

                                </option>
                                @endforeach

                            </select>
                        </div>

                        @error('brand_name')
                        <span class="text text-danger">
                            {{ $message }}
                        </span>
                        @enderror

                    </div>

                    <!-- category name -->
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                            <select
                                class="form-control"
                                name="category_id"
                                id=""
                            >

                                <option value="">Please select a category.</option>

                                @foreach($categories as $category)

                                <option
                                    value="{{ $category->id }}"

                                    @if(isset($product) && $product->category_id == $category->id)
                                    selected
                                    @endif

                                    >

                                    {{ $category->ancestors->count() ? implode(' > ', $category->ancestors->pluck('name')->toArray()) . ' > ' : ' ' }}
                                    {{ $category->name }}

                                </option>
                                @endforeach

                            </select>
                        </div>

                        @error('category_id')
                        <span class="text text-danger">
                            {{ $message }}
                        </span>
                        @enderror

                    </div>

                    <!-- product image -->
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                            <label class="">Product Image</label>
                            <img
                                class="m-2"
                                width="60"
                                src="{{ isset($product) ? asset('products/uploads/' . $product->image) : old('image') }}"
                                alt="{{ isset($product) ? $product->image : '' }}"
                            >
                            <input
                                type="file"
                                name="image"
                                id=""
                                class="form-control"
                            >
                        </div>

                        @error('image')
                        <span class="text text-danger">
                            {{ $message }}
                        </span>
                        @enderror

                    </div>

                    <!-- stock -->
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                            <input
                                class="mdl-textfield__input"
                                type="number"
                                id="stock"
                                name="stock"
                                value="{{ isset($product) ? $product->stock : old('stock') }}"
                            >
                            <label class="mdl-textfield__label"> Stock </label>
                        </div>

                        @error('stock')
                        <span class="text text-danger">
                            {{ $message }}
                        </span>
                        @enderror

                    </div>

                    <!-- price -->
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                            <input
                                class="mdl-textfield__input"
                                type="number"
                                id="price"
                                name="price"
                                step="any"
                                value="{{ isset($product) ? $product->price : old('price')}}"
                            >
                            <label class="mdl-textfield__label">Price</label>
                        </div>

                        @error('price')
                        <span class="text text-danger">
                            {{ $message }}
                        </span>
                        @enderror

                    </div>

                    <!-- actions -->
                    <div class="col-lg-12 p-t-20 text-center">
                        <button
                            type="submit"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink"
                        >

                        {{ isset($product) ? 'Update' : 'Create' }}

                        </button>
                        <a
                            href="{{ route('product.index') }}"
                            type="button"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default"
                        >Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
