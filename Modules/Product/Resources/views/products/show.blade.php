@extends('product::layouts.master')

@section('title')

@endsection

@section('stylesheets')
<script type="text/javascript">
    function VoucherSourcetoPrint(source) {
        return "<html><head><script>function step1(){\n" +
                "setTimeout('step2()', 10);}\n" +
                "function step2(){window.print();window.close()}\n" +
                "</scri" + "pt></head><body onload='step1()'>\n" +
                "<img src='" + source + "' /> <br/><br/> <img src='" + source + "' /></body></html>";
    }
    function VoucherPrint(source) {
        Pagelink = "about:blank";
        var pwa = window.open(Pagelink, "_new");
        pwa.document.open();
        pwa.document.write(VoucherSourcetoPrint(source));
        pwa.document.close();
    }
</script>
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PROFILE SIDEBAR -->
        <div class="profile-sidebar">
            <div class="card card-topline-aqua">
                <div class="card-body no-padding height-9">
                    <div class="row">
                        <div class="course-picture">
                            <img
                                src="{{ asset('products/uploads/'.$product->image) }}"
                                class="img-responsive"
                                alt=""
                            >
                        </div>
                    </div>
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name"> {{ $product->product_name }} </div>
                        <div class="profile-content">
                            <div class="card">
                                <div
                                    class="card-body"
                                    style="margin-right:0;"
                                    id="myText"
                                >
                                    <img
                                        src="{{ asset('products/barcodes/' . $product->barcode) }}"
                                        alt="{{ $product->barcode }}"
                                        width="100%"
                                    >
                                </div>
                            </div>
                            <div class="">
                                <a
                                    href=""
                                    onclick="VoucherPrint('{{ asset('products/barcodes/' . $product->barcode) }}')"
                                    class="btn btn-sm btn-info"
                                >Print a Barcode</a>
                            </div>
                        </div>
                    </div>
                    <!-- END SIDEBAR USER TITLE -->
                </div>
            </div>
        </div>
        <!-- BEGIN PROFILE CONTENT -->
        <div class="profile-content">
            <div class="row">
                <div class="card">
                    <div class="card-head card-topline-aqua">
                        <header>About Product</header>
                    </div>
                    <div class="card-body no-padding height-9">
                        <div class="profile-desc">
                            Open after rule place He earth earth good called days unto which wherein day
                            doeadfasdfasd
                            fish days forth for evening whose his make his bearing years gathering good
                            brought without.
                        </div>
                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b>Stock </b>
                                <div class="profile-desc-item pull-right">{{ $product->stock }}</div>
                            </li>
                            <li class="list-group-item">
                                <b>Price </b>
                                <div class="profile-desc-item pull-right">Rs. {{ $product->price }}</div>
                            </li>
                            <li class="list-group-item">
                                <b> Brand </b>
                                <div class="profile-desc-item pull-right">{{ $product->brand_name }}</div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PROFILE CONTENT -->
    </div>
</div>

@endsection
