<div class="page-footer">
    <div class="page-footer-inner"> <?php echo date('Y'); ?> &copy; productManagement || All Rights reserved
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
