<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('product')->middleware('auth')->group(function () {
    Route::get('/', ['as' => 'product.index', 'uses' => 'ProductController@index']);

    Route::get('create', ['as' => 'product.create', 'uses' => 'ProductController@create']);

    Route::post('store', ['as' => 'product.store', 'uses' => 'ProductController@store']);

    Route::get('show/{id}', ['as' => 'product.show', 'uses' => 'ProductController@show']);

    Route::get('edit/{id}', ['as' => 'product.edit', 'uses' => 'ProductController@edit']);

    Route::put('update/{id}', ['as' => 'product.update', 'uses' => 'ProductController@update']);

    Route::get('delete/{id}', ['as' => 'product.delete', 'uses' => 'ProductController@delete']);
});
