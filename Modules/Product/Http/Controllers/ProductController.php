<?php
namespace Modules\Product\Http\Controllers;

use Picqer;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\File;
use Modules\Product\Entities\Product;
use Illuminate\Support\Facades\Storage;
use Modules\Order\Entities\OrderDetail;
use Illuminate\Contracts\Support\Renderable;
use Modules\Product\Repositories\ProductInterface;
use Modules\Category\Repositories\CategoryInterface;
use Modules\Catalog\Repositories\Brand\BrandInterface;
use Modules\Product\Http\Requests\Product\CreateProductRequest;
use Modules\Product\Http\Requests\Product\UpdateProductRequest;

class ProductController extends Controller
{
    protected $productRepository;

    protected $categoryRepository;

    protected $brandRepository;

    public function __construct(
        ProductInterface $productRepository,
        CategoryInterface $categoryRepository,
        BrandInterface $brandRepository
    ) {
        $this->productRepository = $productRepository;

        $this->categoryRepository = $categoryRepository;

        $this->brandRepository = $brandRepository;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $products = $this->productRepository->getAllProducts();

        return view('product::products.index', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $categories = $this->categoryRepository->getAllCategories();

        $brands = $this->brandRepository->getAllBrands();

        return view('product::products.create', [
            'categories' => $categories,
            'brands' => $brands
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param CreateProductRequest $request
     * @return Renderable
     */
    public function store(CreateProductRequest $request, Product $product)
    {
        if ($request->file('image')) {
            $file = $request->file('image');

            $filename = date('YmdHi') . '-' . $file->getClientOriginalName();

            $file->move(public_path('/products/uploads'), $filename);
        }

        $product_code = $this->productRepository->getBarCodeNumber();

        $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
        file_put_contents('products/barcodes/' . $product_code . '.png', $generator->getBarCode($product_code, $generator::TYPE_CODE_128, 3, 50));

        $product = new Product;
        $product->barcode = $product_code . '.png';
        $product->brand_name = $request->brand_name;
        $product->category_id = $request->category_id;
        $product->price = $request->price;
        $product->product_code = $product_code;
        $product->product_name = $request->product_name;
        $product->stock = $request->stock;
        $product->image = $filename;
        $product->save();

        Toastr::success('Product added successfully :)');

        return redirect(route('product.index'));
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $product = $this->productRepository->getProduct($id);

        return view('product::products.show', ['product' => $product]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $product = $this->productRepository->getProduct($id);

        $brands = $this->brandRepository->getAllBrands();

        $categories = $this->categoryRepository->getAllCategories();

        return view(
            'product::products.create',
            [
                'product' => $product,
                'brands' => $brands,
                'categories' => $categories,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     * @param UpdateProductRequest $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateProductRequest $request, $id)
    {
        $product = $this->productRepository->getProduct($id);

        if ($request->file('image')) {
            $imagePath = public_path() . '/products/uploads/' . $product->image;
            @unlink($imagePath);

            $file = $request->file('image');

            $filename = date('YmdHi') . '-' . $file->getClientOriginalName();

            $file->move(public_path('/products/uploads'), $filename);

            $product->image = $filename;
        }

        $product->product_name = $request->product_name;
        $product->brand_name = $request->brand_name;
        $product->category_id = $request->category_id;
        $product->stock = $request->stock;
        $product->price = $request->price;
        $product->save();

        Toastr::success('Product updated successfully :)');

        return redirect(route('product.index'));
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function delete($id)
    {
        $product = Product::findOrFail($id);

        $orderDetails = OrderDetail::where('product_id', $id)->first();

        if ($orderDetails) {
            Toastr::info('Remove your orders to delete this product.');
        } else {
            File::delete(public_path('/products/uploads/' . $product->image));

            $product->delete();

            Toastr::success('Product deleted successfully :)');
        }

        return redirect(route('product.index'));
    }
}
