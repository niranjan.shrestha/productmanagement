<?php
namespace Modules\Product\Repositories;

interface ProductInterface
{
    public function getAllProducts();

    public function getProduct($id);

    public function countAllProducts();

    public function getBarCodeNumber();
}
