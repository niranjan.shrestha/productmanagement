<?php
namespace Modules\Product\Repositories;

use Modules\Product\Entities\Product;
use Modules\Product\Repositories\ProductInterface;

class ProductRepository implements ProductInterface
{
    public function getAllProducts()
    {
        return Product::all();
    }

    public function getProduct($id)
    {
        return Product::where('id', $id)->first();
    }

    public function countAllProducts()
    {
        return Product::all()->count();
    }

    public function getBarCodeNumber()
    {
        $barcodeNumber = mt_rand(100000000, 999999999);
        if ($this->barcodeNumberExists($barcodeNumber)) {
            return $this->getBarCodeNumber();
        }

        return $barcodeNumber;
    }

    public function barcodeNumberExists($barcodeNumber)
    {
        return Product::where('product_code', $barcodeNumber)->exists();
    }
}
