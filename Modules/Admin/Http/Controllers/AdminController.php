<?php
namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Contracts\Support\Renderable;
use Modules\Product\Repositories\ProductInterface;
use Modules\Category\Repositories\CategoryInterface;
use Modules\Catalog\Repositories\Brand\BrandInterface;
use Modules\Order\Repositories\Transaction\TransactionInterface;

class AdminController extends Controller
{
    protected $productRepository;

    protected $brandRepository;

    protected $categoryRepository;

    protected $transactionRepository;

    public function __construct(
        ProductInterface $productRepository,
        BrandInterface $brandRepository,
        CategoryInterface $categoryRepository,
        TransactionInterface $transactionRepository
    ) {
        $this->productRepository = $productRepository;

        $this->brandRepository = $brandRepository;

        $this->categoryRepository = $categoryRepository;

        $this->transactionRepository = $transactionRepository;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $totalProducts = $this->productRepository->countAllProducts();

        $totalBrands = $this->brandRepository->countAllBrands();

        $totalCategories = $this->categoryRepository->countAllCategories();

        $totalSales = $this->transactionRepository->getAllTransaction()->sum('transaction_amount');

        return view('admin::dashboard.index', [
            'totalProducts' => $totalProducts,
            'totalBrands' => $totalBrands,
            'totalCategories' => $totalCategories,
            'totalSales' => $totalSales
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
