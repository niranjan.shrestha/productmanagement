<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
@include('admin::partials._head')

<!-- END HEAD -->

<body
    class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-blue logo-blue blue-sidebar-color">

    <div class="page-wrapper">

        <!-- start header -->
        @include('admin::partials._header')
        <!-- end header -->

        <div class="page-container">
            <!-- start left sidebar -->
            @include('common-sidebar._sidebar-container')

            <!-- start page content -->
            <div class="page-content-wrapper">

                <div class="page-content">

                    <!-- breadcrumb  -->
                    @include('admin::partials._breadcrumb')

                    <!-- start page container -->
                    @yield('content')
                    <!-- end page container -->

                </div>
            </div>
        </div>
        <!-- end page content -->

        <!-- start footer -->
        @include('admin::partials._footer')
    </div>
    <!-- end footer -->

    <!-- start js include path -->
    @include('admin::partials._scripts')
</body>

</html>
