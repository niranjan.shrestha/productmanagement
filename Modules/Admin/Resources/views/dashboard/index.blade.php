@extends('admin::layouts.master')

@section('title')
Admin Dashboard
@endsection

@section('content')
<!-- start widget -->
<div class="state-overview">
    <div class="row">
        <div class="col-xl-3 col-md-6 col-12">
            <div class="info-box bg-b-green">
                <span class="info-box-icon push-bottom"><i class="fa fa-bitbucket"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Products</span>
                    <span class="info-box-number">
                        {{ $totalProducts }}
                    </span>
                    <div class="progress">
                        <div
                            class="progress-bar"
                            style="width: 45%"
                        ></div>
                    </div>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-xl-3 col-md-6 col-12">
            <div class="info-box bg-b-yellow">
                <span class="info-box-icon push-bottom"><i class="fa fa-folder-open"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Categories</span>
                    <span class="info-box-number">
                        {{ $totalCategories }}
                    </span>
                    <div class="progress">
                        <div
                            class="progress-bar"
                            style="width: 40%"
                        ></div>
                    </div>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-xl-3 col-md-6 col-12">
            <div class="info-box bg-b-blue">
                <span class="info-box-icon push-bottom"><i class="fa fa-rebel"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Brands</span>
                    <span class="info-box-number">
                        {{ $totalBrands }}
                    </span>
                    <div class="progress">
                        <div
                            class="progress-bar"
                            style="width: 85%"
                        ></div>
                    </div>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-xl-3 col-md-6 col-12">
            <div class="info-box bg-b-pink">
                <span class="info-box-icon push-bottom"><i class="fa fa-money"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Total Sales</span>
                    <span class="info-box-number">Rs. {{ $totalSales }}</span>
                    <div class="progress">
                        <div
                            class="progress-bar"
                            style="width: 50%"
                        ></div>
                    </div>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
</div>
<!-- end widget -->
@endsection
