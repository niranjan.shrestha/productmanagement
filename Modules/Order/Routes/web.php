<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('order')->middleware('auth')->group(function () {
    Route::get('/', ['as' => 'order.index', 'uses' => 'OrderController@index']);
    Route::post('/store', ['as' => 'order.store', 'uses' => 'OrderController@store']);
});

Route::prefix('sales')->middleware('auth')->group(function () {
    Route::get('/', ['as' => 'sales.index', 'uses' => 'SalesController@index']);

    Route::get('/delete/{id}', ['as' => 'sales.delete', 'uses' => 'SalesController@delete']);

    Route::get('/weekly', ['as' => 'sales.weekly', 'uses' => 'SalesController@weekly']);

    Route::get('/daily', ['as' => 'sales.daily', 'uses' => 'SalesController@daily']);

    Route::get('/monthly', ['as' => 'sales.monthly', 'uses' => 'SalesController@monthly']);

    Route::get('/yearly', ['as' => 'sales.yearly', 'uses' => 'SalesController@yearly']);
});
