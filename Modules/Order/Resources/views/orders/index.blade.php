@extends('order::layouts.master')

@section('title')
Products order
@endsection

@section('stylesheets')
<script
    language="javascript"
    type="text/javascript"
>
    function removeSpaces(string) {
        return string.split(' ').join('');
    }
</script>
@endsection

{{-- for tooltip --}}
<link
    rel="stylesheet"
    href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
    crossorigin="anonymous"
>

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card card-topline-red">
            <div class="card-head">
                <header> Order Products </header>
            </div>
            <div class="card-body ">

                @livewire('order')

                <div class="modal">
                    <div id="print">
                        @include('order::reports.receipt')
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
    window.setTimeout("document.getElementById('successMessage').style.display='none';", 5000);
</script>

{{-- for tooltip --}}
<script
    src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
    integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"
>
</script>
<script
    src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"
>
</script>
<script
    src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"
>
</script>

<script>
    $(document).ready(function(){
        $('[data-popup="tooltip"]').tooltip();
    });
</script>

<!-- print section -->
<script>
    function PrintReceiptContent(e){
        var data = '<input type="button" id="printPageButton" class="printPageButton" ' +
                'style = "display:block; width:100%; border:none; background-color: #008B8B; color:#fff; ' +
                'padding: 14px 28px; font-size:16px; cursor:pointer; text-align:center" ' +
                ' value="Print Receipt" onclick="printAndHide()" />';
            data += document.getElementById(e).innerHTML;
            myReceipt = window.open("", "myWin", "left=550, top=130, width=400, height=400");
            myReceipt.screnX = 0;
            myReceipt.screnY = 0;
            myReceipt.document.write(data);
            myReceipt.document.title = "Print receipt."
            myReceipt.focus();
    }

    function printAndHide(){
        $('#printPageButton').hide();
        window.print();
    }

</script>

@endsection
