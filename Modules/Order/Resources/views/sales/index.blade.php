@extends('order::layouts.master')

@section('title')
Sales report
@endsection

@section('stylesheets')
<!-- data tables -->
<link
    href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.css"
    rel="stylesheet"
/>
<link
    href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.css"
    rel="stylesheet"
/>
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card card-topline-red">
            <div class="card-head row">
                <div class="col-6">
                    <header>
                        @if (Route::current()->getName() == 'sales.daily')
                        Last day's
                        @elseif (Route::current()->getName() == 'sales.weekly')
                        Weekly
                        @elseif (Route::current()->getName() == 'sales.monthly')
                        Monthly
                        @elseif (Route::current()->getName() == 'sales.yearly')
                        Yearly
                        @endif
                        Report
                    </header>
                </div>

                <div class="col-6 row">
                    <div class="d-flex justify-content-end mb-3">
                        <a
                            href="{{ route('sales.daily') }}"
                            class="btn btn-warning btn-xs mr-2 mt-3"
                        >
                            Last Day's Report
                        </a>

                        {{-- <a
                            href="{{ route('sales.weekly') }}"
                            class="btn btn-info btn-xs mr-2 mt-3"
                        >
                            Weekly Report
                        </a> --}}

                        <a
                            href="{{ route('sales.monthly') }}"
                            class="btn btn-success btn-xs mr-2 mt-3"
                        >
                            Monthly Report
                        </a>

                        <a
                            href="{{ route('sales.yearly') }}"
                            class="btn btn-danger btn-xs mt-3"
                        >
                            Yearly Report
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body ">
                <div class="table-scrollable">
                    <table
                        class="table table-striped table-bordered table-hover table-checkable order-column"
                        style="width: 100%"
                        id="example4"
                    >

                        @if (isset($sales) ? count($sales) > 0 : '')

                        <thead>
                            <tr>
                                <th width="5%"> S.N. </th>
                                <th> Year </th>

                                @if (
                                !( Route::current()->getName() == 'sales.yearly') &&
                                !(Route::current()->getName() == 'sales.monthly') &&
                                !(Route::current()->getName() == 'sales.weekly')
                                )
                                <th> Day </th>
                                <th> Order no. </th>
                                <th> Product name </th>
                                <th> Unit Price </th>
                                @endif

                                <th> Quantity </th>
                                <th> Total amount </th>

                                @if (
                                !(Route::current()->getName() == 'sales.weekly') &&
                                !(Route::current()->getName() == 'sales.monthly') &&
                                !(Route::current()->getName() == 'sales.yearly')
                                )
                                <th>
                                    Time
                                </th>
                                @endif

                                @if (
                                !(Route::current()->getName() == 'sales.yearly') &&
                                !(Route::current()->getName() == 'sales.monthly') &&
                                !(Route::current()->getName() == 'sales.weekly')
                                )
                                <th
                                    @if ( (Route::current()->getName() == ''))

                                    width="17%"

                                    @else

                                    width = "5%"

                                    @endif
                                    > Actions </th>
                                @endif

                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($sales as $key=>$sale)

                            @if (
                            Request::segment(2) == '' ||
                            Request::segment(2) == 'daily'
                            )
                            <tr>
                                <td>
                                    {{ $loop->iteration }}
                                </td>

                                <td>
                                    {{ date_format($sale->created_at, "Y") }}
                                </td>

                                <td>
                                    {{ date_format($sale->created_at, "dS, M") }}
                                </td>

                                <td>
                                    {{ $sale->order_id }}
                                </td>

                                <td>
                                    {{ $sale->product->product_name }}
                                </td>

                                <td>
                                    {{ $sale->quantity }}
                                </td>

                                <td>
                                    {{ $sale->unit_price }}
                                </td>

                                <td>
                                    Rs. {{ $sale->amount }}
                                </td>

                                <td>
                                    {{ date_format($sale->created_at, "h:i:s A") }}
                                </td>

                                <td class="d-flex justify-content-center">
                                    <a
                                        class="btn btn-xs btn-danger ml-3 deleteSales"
                                        data-toggle="modal"
                                        data-target="#deleteSales"
                                        title="Delete Sales record?"
                                        link="{{ route('sales.delete', $sale->id) }}"
                                    >
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            @endif

                            {{-- @if (Request::segment(2) == 'weekly') --}}

                            {{-- <tr>
                                <td>
                                    {{ $loop->iteration }}
                                </td>

                                <td> --}}
                                    {{-- @php
                                        $day = explode("-",$key);
                                    @endphp --}}

                                    {{-- {{ $day[0] . "-" . $day[2] }} --}}
                                    {{-- {{ $key }}<sup>th</sup> week
                                </td>

                                <td>
                                    {{ $sale->sum('quantity') }}
                                </td>

                                <td>
                                    Rs. {{ $sale->sum('amount') }}
                                </td>
                            </tr> --}}

                            {{-- @endif --}}

                            @if (Request::segment(2) == 'yearly')

                            <tr>
                                <td>
                                    {{ $loop->iteration }}
                                </td>

                                <td>
                                    {{ $key }}
                                </td>

                                <td>
                                    {{ $sale->sum('quantity') }}
                                </td>

                                <td>
                                    Rs. {{ $sale->sum('amount') }}
                                </td>
                            </tr>

                            @endif

                            @if (Request::segment(2) == 'monthly')

                            <tr>
                                <td>
                                    {{ $loop->iteration }}
                                </td>

                                <td>
                                    {{ $key }}
                                </td>

                                <td>
                                    {{ $sale->sum('quantity') }}
                                </td>

                                <td>
                                    Rs. {{ $sale->sum('amount') }}
                                </td>
                            </tr>

                            @endif

                            @endforeach

                        </tbody>

                        @else

                        <h1 class="text-center">
                            No sales yet.
                        </h1>

                        @endif

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Sales delete Modal -->
<div
    class="modal fade"
    id="deleteSales"
    tabindex="-1"
    role="dialog"
    aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true"
>
    <div
        class="modal-dialog modal-dialog-centered"
        role="document"
    >
        <div class="modal-content">
            <div class="modal-header">
                <h5
                    class="modal-title"
                    id="exampleModalLongTitle"
                >
                    <b>
                        Be careful what you wish for.
                    </b>
                </h5>
                <button
                    type="button"
                    class="close"
                    data-dismiss="modal"
                    aria-label="Close"
                >
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                This action can't be reverted. The seleted sales record will be deleted forever. Are you sure?
            </div>
            <div class="modal-footer">

                <a
                    class="btn btn-danger getSalesDeleteLink"
                >
                    Yes, Delete it.
                </a>

                <button
                    type="button"
                    class="btn btn-secondary"
                    data-dismiss="modal"
                >
                    No, Go back.
                </button>

            </div>
        </div>
    </div>
</div>

@endsection


@section('scripts')
<!-- data tables -->
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/table/table_data.js') }}"></script>

<!-- sending delete link to modal -->
<script>
    $('.deleteSales').on('click', function(){
        var getDeleteLink = $(this).attr('link');

        $('.getSalesDeleteLink').attr('href', getDeleteLink);
    });
</script>
@endsection
