<div class="page-header navbar navbar-fixed-top">
    <div class="page-header-inner ">
        <!-- logo start -->
        <div class="page-logo">
            <a href="{{ route('admin.dashboard') }}">
                <span class="logo-icon fa-rotate-45">
                    <i class="fa fa-product-hunt"></i>
                </span>
                <span class="logo-default">MS</span> </a>
        </div>
        <!-- logo end -->
        <ul class="nav navbar-nav navbar-left in">
            <li><a
                    href="#"
                    class="menu-toggler sidebar-toggler"
                ><i class="icon-menu"></i></a></li>
        </ul>

        <!-- start mobile menu -->
        <a
            href="javascript:;"
            class="menu-toggler responsive-toggler"
            data-toggle="collapse"
            data-target=".navbar-collapse"
        >
            <span></span>
        </a>
        <!-- end mobile menu -->
    </div>
</div>
