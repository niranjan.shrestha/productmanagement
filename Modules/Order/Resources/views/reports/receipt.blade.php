<!-- stylesheets -->

<style>
    #invoice {
        box-shadow: 0 0 1in -0.25in rgb(0, 0, 0);
        padding: 2mm;
        margin: 0 auto;
        width: 58mm;
        background: #fff;
    }

    #invoice::selection {
        background: rgb(255, 246, 122);
        color: rgb(0, 0, 0);
    }

    #invoice::-moz-selection {
        background: rgb(255, 246, 122);
        color: rgb(0, 0, 0);
    }

    #invoice h1 {
        font-size: 1.5em;
        color: #222;
    }

    #invoice h2 {
        font-size: 0.9em;
    }

    #invoice h3 {
        font-size: 0.6em;
        font-weight: 300;
        line-height: 2em;
    }

    #invoice p {
        font-size: 0.7em;
        line-height: 1.2em;
        color: #666;
    }

    #invoice #top,
    #invoice #mid,
    #invoice #bot {
        border-bottom: 1px solid #eee;
    }

    #invoice #top {
        min-height: 100px;
    }

    #invoice #mid {
        min-height: 80px;
    }

    #invoice #bot {
        min-height: 50px;
    }

    #invoice #top .logo {
        height: 60px;
        width: 60px;
        background-image: url() no-repeat;
        background-size: 60px 60px;
        border-radius: 50%;
    }

    #invoice .info {
        font-size: .9em;
        display: block;
        margin-left: 0;
        text-align: center;
    }

    #invoice .title {
        text-align: right;
    }

    #invoice .title {
        float: right;
    }

    #invoice .title p {
        text-align: right;
    }

    #invoice table {
        width: 100%;
        border-collapse: collapse;
    }

    #invoice .table-title {
        font-size: 0.5em;
        background: #eee;
    }

    #invoice .service {
        border-bottom: 1px solid #eee;
    }

    #invoice .item {
        width: 24mm;
    }

    #invoice .item-text {
        font-size: 0.5em;
    }

    #invoice #legal-copy {
        margin-top: 5mm;
    }

    .legal {
        text-align: center;
    }

    .serial-number {
        margin-top: 5mm;
        margin-bottom: 2mm;
        text-align: center;
        font-size: 12px;
    }

    .serial {
        font-size: 10px !important;
    }

</style>
<!-- print section -->
<div id="invoice">
    <div id="printed-content">
        <center id="logo">
            <div class="top">
                LOGO
            </div>
            <div class="info">
                store information
            </div>
            <h2>
                productmgnt store
            </h2>
        </center>
    </div>

    <div class="mid">
        <div class="info">
            <h2>
                Contact Us
            </h2>
            <p>
                Address: Purano Baneshwor near Apex College <br>
                Email: productmanagement@gmail.com <br>
                Phone: 9809980998
            </p>
        </div>
    </div>

    <div class="bot">
        <div id="table">
            <table>
                <tr class="table-title">
                    <td
                        class="item"
                        style="text-align: center;"
                    >
                        <h2>
                            Item
                        </h2>
                    </td>
                    <td class="hour">
                        <h2>
                            Quantity
                        </h2>
                    </td>
                    <td class="rate">
                        <h2>
                            Price
                        </h2>
                    </td>
                    <td class="rate">
                        <h2>
                            Sub total
                        </h2>
                    </td>
                </tr>

                @foreach ($order_receipt as $receipt)

                <tr class="service">
                    <td class="table-item">
                        <p
                            class="item-text"
                            style="text-align: center;"
                        >
                            {{ $receipt->product->product_name }}
                        </p>
                    </td>
                    <td class="table-item">
                        <p
                            class="item-text"
                            style="text-align: center;"
                        >
                            {{ $receipt->quantity }}
                        </p>
                    </td>
                    <td class="table-item">
                        <p class="item-text">
                            Rs. {{ number_format($receipt->unit_price, 2) }}
                        </p>
                    </td>
                    <td class="table-item">
                        <p
                            class="item-text"
                            style="text-align: center;"
                        >
                            Rs. {{ number_format($receipt->amount, 2) }}
                        </p>
                    </td>
                </tr>

                @endforeach

                <!-- service charges and taxes (optional) -->
                {{-- <tr class="table-title">
                    <td></td>
                    <td></td>
                    <td class="rate">
                        <p class="item-text">
                            something
                        </p>
                    </td>
                    <td class="payment">
                        <p class="item-text">
                            Rs.
                        </p>
                    </td>
                </tr> --}}

                <tr class="table-title">
                    <td></td>
                    <td></td>
                    <td class="rate">
                        Total (Rs.)
                    </td>
                    <td class="payment">
                        {{ number_format($order_receipt->sum('amount'), 2) }}
                    </td>
                </tr>
            </table>

            <div class="legal-copy">
                <p class="legal">
                    <strong>
                        ** Thank you for visiting. **
                    </strong>
                    <br />
                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Et praesentium aliquid incid
                </p>
            </div>

            <div class="serial-number">
                Serial : <span class="serial">xxx-xxxx-xxx</span>
                <span>
                    23/23/2020 &nbsp; &nbsp; 00:00
                </span>
            </div>
        </div>
    </div>
</div>
