<?php
namespace Modules\Order\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Modules\Order\Entities\OrderDetail;
use Illuminate\Contracts\Support\Renderable;
use Modules\Order\Repositories\OrderDetail\OrderDetailInterface;

class SalesController extends Controller
{
    protected $orderDetailRepository;

    public function __construct(
        OrderDetailInterface $orderDetailRepository
    ) {
        $this->orderDetailRepository = $orderDetailRepository;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $sales = $this->orderDetailRepository->getAllSales();

        return view(
            'order::sales.index',
            ['sales' => $sales]
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('order::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('order::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function delete($id)
    {
        $orderDetail = OrderDetail::findOrFail($id);

        $orderDetail->delete();

        Toastr::success('Sale record deleted successfully :)');

        return redirect(route('sales.index'));
    }

    public function daily()
    {
        $sales = $this->orderDetailRepository->getAllDailySales();

        return view(
            'order::sales.index',
            ['sales' => $sales]
        );
    }

    public function weekly()
    {
        $sales = $this->orderDetailRepository->fetchWeeklyReport();

        return view(
            'order::sales.index',
            ['sales' => $sales]
        );
    }

    public function monthly()
    {
        $sales = $this->orderDetailRepository->fetchMonthlyReport();

        return view(
            'order::sales.index',
            ['sales' => $sales]
        );
    }

    public function yearly()
    {
        $sales = $this->orderDetailRepository->fetchYearlyReport();

        return view(
            'order::sales.index',
            ['sales' => $sales]
        );
    }
}
