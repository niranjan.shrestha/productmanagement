<?php
namespace Modules\Order\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Order\Entities\Cart;
use Modules\Order\Entities\Order;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Brian2694\Toastr\Facades\Toastr;
use Modules\Product\Entities\Product;
use Modules\Order\Entities\OrderDetail;
use Modules\Order\Entities\Transaction;
use Illuminate\Contracts\Support\Renderable;
use Modules\Product\Repositories\ProductInterface;
use Modules\Order\Http\Requests\Order\CreateOrderRequest;

class OrderController extends Controller
{
    protected $productRepository;

    public function __construct(ProductInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $products = $this->productRepository->getAllProducts();

        $orders = Order::all();

        // last order details
        $lastId = OrderDetail::max('order_id');
        $order_receipt = OrderDetail::where('order_id', $lastId)->get();

        return view('order::orders.index', [
            'products' => $products,
            'orders' => $orders,
            'order_receipt' => $order_receipt,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('order::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(CreateOrderRequest $request)
    {
        if ($request->grand_total > $request->paid_amount) {
            Toastr::error('Your payment is less than total amount. :(');
        } else {
            DB::transaction(function () use ($request) {
                // first storing incoming data in order model
                $order = new Order;
                $order->name = $request->customer_name;
                $order->phone = $request->customer_phone;
                $order->save();

                $order_id = $order->id;

                // now storind data in order details model
                for ($product_id = 0; $product_id < count($request->product_id); $product_id++) {
                    $orderDetail = new OrderDetail;
                    $orderDetail->order_id = $order_id;
                    $orderDetail->product_id = $request->product_id[$product_id];
                    $orderDetail->unit_price = $request->price[$product_id];
                    $orderDetail->quantity = $request->quantity[$product_id];
                    $orderDetail->amount = $request->total_amount[$product_id];
                    $orderDetail->save();
                }

                // lastly storing in transactions table
                $transaction = new Transaction;

                $transaction->create([
                    'order_id' => $order_id,
                    'returned_amount' => $request->returning_amount,
                    'paid_amount' => $request->paid_amount,
                    'transaction_amount' => $orderDetail->amount,
                    'transaction_date' => date('Y-m-d'),
                ]);

                // deducting from stock from product table
                for ($product_id = 0; $product_id < count($request->product_id); $product_id++) {
                    $product = Product::where('id', $request->product_id[$product_id])->first();

                    $product->stock = $product->stock - $request->quantity[$product_id];

                    $product->save();
                }

                Cart::truncate();

                // Last order history
                $products = Product::all();

                $order_details = OrderDetail::where('order_id', $order_id)->get();

                $orderedBy = Order::where('id', $order_id)->get();

                return view(
                    'order::orders.index',
                    [
                        'products' => $products,
                        'order_details' => $order_details,
                        'customer_orders' => $orderedBy
                    ]
                );
            });

            Toastr::success('Order made successfully :)');
        }

        return redirect(route('order.index'));
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('order::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('order::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
