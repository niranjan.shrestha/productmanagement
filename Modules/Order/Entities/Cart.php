<?php
namespace Modules\Order\Entities;

use Modules\Product\Entities\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Cart extends Model
{
    protected $table = 'carts';

    protected $fillable = [
        'product_id',
        'product_quantity',
        'product_price',
        'user_id'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
