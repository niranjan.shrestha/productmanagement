<?php
namespace Modules\Order\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Transaction extends Model
{
    protected $table = 'transactions';

    protected $fillable = [
        'order_id',
        'payment_method',
        'paid_amount',
        'returned_amount',
        'transaction_amount',
        'transaction_date',
        'user_id',
    ];
}
