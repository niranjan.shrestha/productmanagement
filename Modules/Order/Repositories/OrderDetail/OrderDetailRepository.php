<?php
namespace Modules\Order\Repositories\OrderDetail;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Modules\Order\Entities\OrderDetail;
use Modules\Order\Repositories\OrderDetail\OrderDetailInterface;

class OrderDetailRepository implements OrderDetailInterface
{
    public function getAllSales()
    {
        return OrderDetail::all();
    }

    public function getAllDailySales()
    {
        return OrderDetail::where('created_at', '>=', Carbon::now()->subdays(1))->get();
    }

    public function fetchWeeklyReport()
    {
        return OrderDetail::all()->groupBy(function ($item) {
            return $item->created_at->format('Y-M-W');
        });
    }

    public function fetchMonthlyReport()
    {
        return OrderDetail::all()->groupBy(function ($item) {
            return $item->created_at->format('Y-M');
        });
    }

    public function fetchYearlyReport()
    {
        return OrderDetail::all()->groupBy(function ($item) {
            return $item->created_at->format('Y');
        });
    }
}
