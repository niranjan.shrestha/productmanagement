<?php
namespace Modules\Order\Repositories\OrderDetail;

interface OrderDetailInterface
{
    public function getAllSales();

    public function getAllDailySales();

    public function fetchWeeklyReport();

    public function fetchMonthlyReport();

    public function fetchYearlyReport();
}
