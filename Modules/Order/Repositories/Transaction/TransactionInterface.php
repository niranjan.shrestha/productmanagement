<?php
namespace Modules\Order\Repositories\Transaction;

interface TransactionInterface
{
    public function getAllTransaction();
}
