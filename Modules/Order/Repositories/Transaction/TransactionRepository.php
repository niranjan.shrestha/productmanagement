<?php
namespace Modules\Order\Repositories\Transaction;

use Modules\Order\Entities\Transaction;

class TransactionRepository implements TransactionInterface
{
    public function getAllTransaction()
    {
        return Transaction::all();
    }
}
