@extends('category::layouts.master')

@section('title')
Category Index
@endsection

@section('stylesheets')
<!-- data tables -->
<link
    href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.css"
    rel="stylesheet"
/>
<link
    href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.css"
    rel="stylesheet"
/>
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card card-topline-red">
            <div class="card-head">
                <header>Category Lists</header>
            </div>
            <div class="card-body ">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-6">
                        <div class="btn-group">
                            <a
                                href="{{ route('category.create') }}"
                                id="addRow1"
                                class="btn btn-info"
                            >
                                Add New <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="table-scrollable">
                    <table
                        class="table table-striped table-bordered table-hover table-checkable order-column"
                        style="width: 100%"
                        id="example4"
                    >

                        @if (count($categories) > 0)

                        <thead>
                            <tr>
                                <th width="5%"> S.N. </th>
                                <th> Category Name </th>
                                <th width="17%"> Actions </th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($categories as $category)
                            <tr>
                                <td>
                                    {{ $loop->iteration }}
                                </td>

                                <td>
                                    {{ $category->ancestors->count() ? implode(' > ', $category->ancestors->pluck('name')->toArray()) . ' > ' : ' ' }}
                                    {{ $category->name }}
                                </td>

                                <td>

                                    <a
                                        href=" {{ route('category.edit', $category->id) }}"
                                        class="btn btn-sm btn-info"
                                    >
                                        Edit
                                    </a>

                                    <!-- Button trigger modal -->
                                    <a
                                        class="btn btn-sm btn-danger deleteCategory"
                                        data-toggle="modal"
                                        data-target="#deleteCategory"
                                        link="{{ route('category.destroy', $category->id) }}"
                                    >
                                        Delete
                                    </a>

                                </td>
                            </tr>

                            @endforeach

                        </tbody>

                        @else

                        <h1 class="text-center">
                            No Categories yet.
                        </h1>

                        @endif

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Category delete Modal -->
<div
    class="modal fade"
    id="deleteCategory"
    tabindex="-1"
    role="dialog"
    aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true"
>
    <div
        class="modal-dialog modal-dialog-centered"
        role="document"
    >
        <div class="modal-content">
            <div class="modal-header">
                <h5
                    class="modal-title"
                    id="exampleModalLongTitle"
                >
                    <b>
                        Be careful what you wish for.
                    </b>
                </h5>
                <button
                    type="button"
                    class="close"
                    data-dismiss="modal"
                    aria-label="Close"
                >
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                This action can't be reverted and the seleted category will be deleted forever. Are you sure?
            </div>
            <div class="modal-footer">

                <a
                    class="btn btn-danger getCategoryDeleteLink"
                >
                    Yes, Delete it.
                </a>

                <button
                    type="button"
                    class="btn btn-secondary"
                    data-dismiss="modal"
                >
                    No, Go back.
                </button>

            </div>
        </div>
    </div>
</div>

@endsection


@section('scripts')
<!-- data tables -->
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/table/table_data.js') }}"></script>

<!-- sending delete link to modal -->
<script>
    $('.deleteCategory').on('click', function(){
        var getDeleteLink = $(this).attr('link');

        $('.getCategoryDeleteLink').attr('href', getDeleteLink);
    });
</script>
@endsection
