@extends('admin::layouts.master')

@section('title')
{{ isset($category) ? 'Update' : 'Add' }} Category
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <div class="card-head">
                <header>{{ isset($category) ? 'Update' : 'Add' }} Category</header>
            </div>

            <form
                action="{{ isset($category) ? route('category.update', $category->id) : route('category.store') }}"
                method="post"
            >
                @csrf

                @if (isset($category))

                @method('PUT')

                @endif

                <div class="card-body row">

                    <div class="col-lg-12 p-t-20">
                        <select
                            name="parent"
                            id="parent"
                            class="form-control"
                        >
                            <option value="">Select a parent</option>

                            @foreach($categories as $parent)

                            <option value="{{ $parent->id }}"

                                @if ($parent->ancestors->count() > 0)

                                @if (isset($category) && $parent->parent_id == $category->id)

                                selected

                                @endif

                                @endif
                                >
                                {{ $parent->ancestors->count() ? implode(' > ', $parent->ancestors->pluck('name')->toArray()) . ' > ' : ' ' }}
                                {{ $parent->name }}

                            </option>
                            @endforeach

                        </select>
                    </div>

                    <div class="col-lg-12 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                            <input
                                class="mdl-textfield__input"
                                type="text"
                                id="category_name"
                                name="name"
                                value="{{ isset($category) ? $category->name : old('category_name') }}"
                            >
                            <label class="mdl-textfield__label">Category Name</label>
                        </div>

                        @error('name')
                        <span class="text-danger">
                            {{ $message }}
                        </span>
                        @enderror

                    </div>

                    <div class="col-lg-12 p-t-20 text-center">
                        <button
                            type="submit"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink"
                        >
                            {{ isset($category) ? 'Update' : 'Create' }}
                        </button>

                        <a
                            href="{{ route('category.index') }}"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default"
                        >Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
