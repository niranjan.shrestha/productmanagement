<?php
namespace Modules\Category\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Modules\Product\Entities\Product;
use Modules\Category\Entities\Category;
use Illuminate\Contracts\Support\Renderable;
use Modules\Category\Repositories\CategoryInterface;
use Modules\Category\Http\Requests\Category\CreateCategoryRequest;
use Modules\Category\Http\Requests\Category\UpdateCategoryRequest;

class CategoryController extends Controller
{
    protected $categoryRepository;

    public function __construct(CategoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $parentCategories = $this->categoryRepository->getAllCategories();

        return view('category::categories.index', ['categories' => $parentCategories]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $parentCategories = $this->categoryRepository->getAllCategories();

        return view('category::categories.create', ['categories' => $parentCategories]);
    }

    /**
     * Store a newly created resource in storage.
     * @param CreateCategoryRequest $request
     * @return Renderable
     */
    public function store(CreateCategoryRequest $request)
    {
        $category = Category::create([
            'name' => $request->name
        ]);

        if ($request->parent !== null) {
            $node = Category::find($request->parent);

            $node->appendNode($category);
        }

        Toastr::success('Category added successfully :)');

        return redirect(route('category.index'));
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('category::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $categories = $this->categoryRepository->getAllCategories();

        $category = Category::findOrFail($id);

        return view('category::categories.create', [
            'categories' => $categories,
            'category' => $category
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param UpdateCategoryRequest $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateCategoryRequest $request, $id)
    {
        $category = Category::findOrFail($id);

        $category->update([
            'name' => $request->name
        ]);

        Toastr::success('Category updated successfully :)');

        return redirect(route('category.index'));
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $category = Category::where('id', $id)->first();

        try {
            $category->delete();

            Toastr::success('Category deleted successfully :)');

            return redirect(route('category.index'));
        } catch (\Throwable $th) {
            Toastr::info('Parent category can\'t be deleted.');

            return redirect(route('category.index'));
        }
    }
}
