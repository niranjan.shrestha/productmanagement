<?php
namespace Modules\Category\Repositories;

use Modules\Category\Entities\Category;

class CategoryRepository implements CategoryInterface
{
    public function getAllCategories()
    {
        return Category::with('ancestors')->get();
    }

    public function countAllCategories()
    {
        return Category::all()->count();
    }
}
