<?php
namespace Modules\Category\Repositories;

interface CategoryInterface
{
    public function getAllCategories();

    public function countAllCategories();
}
