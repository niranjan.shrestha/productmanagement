<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('category')->group(function () {
    Route::get('/', ['as' => 'category.index', 'uses' => 'CategoryController@index']);

    Route::get('create', ['as' => 'category.create', 'uses' => 'CategoryController@create']);

    Route::POST('store', ['as' => 'category.store', 'uses' => 'CategoryController@store']);

    Route::get('edit/{id}', ['as' => 'category.edit', 'uses' => 'CategoryController@edit']);

    Route::put('update/{category}', ['as' => 'category.update', 'uses' => 'CategoryController@update']);

    Route::get('/destroy/{category}', ['as' => 'category.destroy', 'uses' => 'CategoryController@destroy']);
});
