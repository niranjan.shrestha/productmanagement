@php
$param1 = Request::segment(1);
@endphp

<div class="sidebar-container">
    <div class="sidemenu-container navbar-collapse collapse fixed-menu">
        <div
            id="remove-scroll"
            class="left-sidemenu"
        >

            <ul
                class="sidemenu  page-header-fixed slimscroll-style"
                data-keep-expanded="false"
                data-auto-scroll="true"
                data-slide-speed="200"
                style="padding-top: 20px"
            >

                <li class="sidebar-toggler-wrapper hide">
                    <div class="sidebar-toggler">
                        <span></span>
                    </div>
                </li>

                <!-- dashboard -->
                <li class="nav-item
                {{ $param1 == 'admin' ? 'active' : '' }}
                open">
                    <a
                        href="{{ route('admin.dashboard') }}"
                        class="nav-link nav-toggle"
                    >
                        <i class="fa fa-dashboard"></i>
                        <span class="title">
                            Dashboard
                        </span>
                    </a>
                </li>

                <!-- product -->
                <li class="nav-item
                    {{ $param1 == 'product' ? 'active' : '' }}
                    open">
                    <a
                        href="{{ route('product.index') }}"
                        class="nav-link nav-toggle"
                    >
                        <i class="fa fa-bitbucket"></i>
                        <span class="title">Product</span>
                    </a>

                    <ul class="sub-menu">
                        <li class="nav-item open">
                            <a
                                href="{{ route('product.index') }}"
                                class="nav-link "
                            >
                                <span class="title">
                                    All Products
                                </span>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a
                                href="{{ route('product.create') }}"
                                class="nav-link"
                            >
                                <span class="title">
                                    Add Product
                                </span>
                            </a>
                        </li>
                    </ul>
                </li>

                <!-- orders -->
                <li class="nav-item
                    {{ $param1 == 'order' ? 'active' : '' }}
                    open">
                    <a
                        href="{{ route('order.index') }}"
                        class="nav-link nav-toggle"
                    >
                        <i class="fa fa-opencart"></i>
                        <span class="title">
                            Product orders
                        </span>
                    </a>
                </li>

                <!-- category -->
                <li class="nav-item
                {{ $param1 == 'category' ? 'active' : '' }}
                open">
                    <a
                        href="{{ route('category.index') }}"
                        class="nav-link nav-toggle"
                    >
                        <i class="fa fa-folder-open-o"></i>
                        <span class="title">Category</span>
                    </a>

                    <ul class="sub-menu">
                        <li class="nav-item open">
                            <a
                                href="{{ route('category.index') }}"
                                class="nav-link "
                            > <span class="title">All
                                    Categories</span>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a
                                href="{{ route('category.create') }}"
                                class="nav-link"
                            >
                                <span class="title">
                                    Add Category
                                </span>
                            </a>
                        </li>

                    </ul>
                </li>

                <!-- catalogs -->
                <li class="nav-item
                {{ $param1 == 'brand' ? 'active' : '' }}
                open">
                    <a
                        href="{{ route('brand.index') }}"
                        class="nav-link nav-toggle"
                    >
                        <i class="fa fa-rebel"></i>
                        <span class="title">Brand</span>
                    </a>

                    <ul class="sub-menu">
                        <li class="nav-item open">
                            <a
                                href="{{ route('brand.index') }}"
                                class="nav-link "
                            > <span class="title"> Brand index </span>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a
                                href="{{ route('brand.create') }}"
                                class="nav-link"
                            >
                                <span class="title">
                                    Create brand
                                </span>
                            </a>
                        </li>
                    </ul>
                </li>

                <!-- sales report -->
                <li class="nav-item
                {{ $param1 == 'sales' ? 'active' : '' }}
                open">
                    <a
                        href="{{ route('sales.index') }}"
                        class="nav-link nav-toggle"
                    >
                        <i class="fa fa-file-text"></i>
                        <span class="title">
                            Sales Report
                        </span>
                    </a>
                </li>

                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>

            </ul>
        </div>
    </div>
</div>
