<div class="row">
    <div class="col-lg-9 col-md-9 col-sm-9">
        <form wire:submit.prevent="insertToCart">
            <div class="input-group">
                <input
                    type="number"
                    class="form-control"
                    placeholder="Enter Product code"
                    wire:model="product_code"
                    autofocus
                    autocomplete="off"
                    onkeyup="this.value=removeSpaces(this.value);"
                >
            </div>
        </form>

        @if ($message)
        <div
            class="alert alert-info"
            id="successMessage"
        >
            {{ $message }}
        </div>
        @endif

        <div class="table-scrollable">
            <table
                class="table table-striped table-bordered table-hover table-checkable order-column"
                style="width: 100%"
                id="example4"
            >

                @if ($productInCart->count() > 0)

                <thead>
                    <tr>
                        <th width="5%"> S.N. </th>
                        <th> Product name </th>
                        <th width="20%"> Quantity </th>
                        <th width="15%"> Price </th>
                        <th width="15%"> Total </th>

                    </tr>
                </thead>

                <tbody class="addMoreProduct">

                    @foreach ($productInCart as $cart)

                    <tr>
                        <td>
                            {{ $loop->iteration }}
                        </td>

                        <td>
                            <input
                                type="text"
                                class="form-control"
                                readonly
                                value="{{ $cart->product->product_name }}"
                            >
                        </td>

                        <td>
                            <div class="input-group spinner">
                                <span class="input-group-btn">
                                    <a
                                        wire:click.prevent="decrementQuantity({{ $cart->id }})"
                                        class="btn btn-info"
                                        type="button"
                                    >
                                        <span class="fa fa-minus"></span>
                                    </a>
                                </span>

                                <input
                                    type="text"
                                    class="form-control text-center"
                                    readonly
                                    value="{{ $cart->product_quantity }}"
                                    min="1"
                                    max="{{ $cart->product->stock }}"
                                />

                                <span class="input-group-btn">
                                    <a
                                        wire:click.prevent="incrementQuantity({{ $cart->id }})"
                                        class="btn btn-danger"
                                        type="button"
                                    >
                                        <span class="fa fa-plus"></span>
                                    </a>
                                </span>
                            </div>
                        </td>

                        <td>
                            <input
                                type="number"
                                readonly
                                class="form-control price"
                                value="{{ $cart->product->price}}"
                            >
                        </td>

                        <td>
                            <input
                                type="number"
                                disabled
                                class="form-control total_amount"
                                value="{{ $cart->product_price }}"
                            >
                        </td>

                        <td>
                            <a
                                href="#"
                                class="btn btn-xm btn-danger"
                                data-popup="tooltip"
                                data-placement="bottom"
                                title="Delete"
                                wire:click="removeProduct({{ $cart->id }})"
                            >
                                <i class="fa fa-minus-circle"></i>
                            </a>
                        </td>

                    </tr>

                    @endforeach

                </tbody>

                @else

                <h1 class="text-center">
                    No Orders yet.
                </h1>

                @endif

            </table>
        </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-3">
        <form
            action="{{ route('order.store') }}"
            method="post"
        >

            @csrf

            @foreach ($productInCart as $cart)

            <input
                type="hidden"
                class="form-control"
                name="product_id[]"
                readonly
                value="{{ $cart->product->id }}"
            >

            <input
                type="hidden"
                class="form-control text-center"
                name="quantity[]"
                readonly
                value="{{ $cart->product_quantity }}"
                min="1"
            />

            <input
                type="hidden"
                name="price[]"
                id="price"
                readonly
                class="form-control price"
                value="{{ $cart->product->price}}"
            >

            <input
                type="hidden"
                name="total_amount[]"
                id="total_amount"
                readonly
                class="form-control total_amount"
                value="{{ $cart->product_price }}"
            >

            @endforeach

            <div class="card card-default">
                <div class="card-header">
                    <b>Total: Rs.</b> <b class="total">
                        {{ isset($productInCart) ? $productInCart->sum('product_price') : '0.00' }} </b>

                    <input
                        type="hidden"
                        name="grand_total"
                        value="{{ isset($productInCart) ? $productInCart->sum('product_price') : '' }}"
                    >
                </div>
                <div class="card-body row">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input
                            class="mdl-textfield__input"
                            type="text"
                            id="txtDepName"
                            name="customer_name"
                            placeholder="Enter customer's name"
                            value="{{ old('customer_name') }}"
                        >

                        @error('customer_name')
                        <span class="text text-danger">
                            <small>
                                {{ $message }}
                            </small>
                        </span>
                        @enderror

                    </div>

                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input
                            class="mdl-textfield__input"
                            type="number"
                            name="customer_phone"
                            placeholder="Customer's phone number"
                            value="{{ old('customer_phone') }}"
                        >

                        @error('customer_phone')
                        <span class="text text-danger">
                            <small>
                                {{ $message }}
                            </small>
                        </span>
                        @enderror

                    </div>

                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input
                            class="mdl-textfield__input"
                            type="number"
                            name="paid_amount"
                            min="0"
                            wire:model="paidMoney"
                            placeholder="Payment"
                        >

                        @error('paid_amount')
                        <span class="text text-danger">
                            <small>
                                {{ $message }}
                            </small>
                        </span>
                        @enderror

                    </div>

                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <small>Returning change</small>
                        <input
                            class="mdl-textfield__input"
                            type="number"
                            readonly
                            name="returning_amount"
                            value=""
                            wire:model="returnedMoney"
                        >
                    </div>

                    <div class="col-lg-12 p-t-20 text-center">
                        <button
                            class="btn btn-sm btn-success"
                            type="submit"
                        >
                            Make an order
                        </button>
                    </div>

                    <div class="col-lg-12 p-t-10 text-center">
                        <button
                            type="button"
                            class="btn btn-xs btn-dark mt-3"
                            onclick="PrintReceiptContent('print')"
                        >
                            <i class="fa fa-print"></i>
                            Print
                        </button>

                        <a
                            href="{{ route('sales.index') }}"
                            class="btn btn-xs btn-danger mt-3"
                        >
                            <i class="fa fa-print"></i>
                            Report
                        </a>
                    </div>
                </div>
            </div>
    </div>
</div>
</form>
